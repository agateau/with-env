_with_env() {
    _arguments \
        {-h,--help}"[display help and exit]" \
        {-l,--list}"[list envs and exit]" \
        {-e,--edit}"[edit ENV file]" \
        "1:env:($(with-env --list))" \
        "2:command: _command_names -e" \
        "*:args:_files"
}

compdef _with_env with-env
