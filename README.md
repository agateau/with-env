# with-env

## Introduction

`with-env` is a simple tool to avoid keeping secrets in always-loaded environment variables.

Secrets are stored in individual files in `~/.config/with-env`. For example you could have a `~/.config/with-env/some-env-name` with this content:

```
export DB_USERNAME=foo
export DB_PASSWORD=bar
export DB_HOST=example.com
```

If you want to run the command `deploy` which needs these environment variables, you run:

```
with-env some-env-name deploy
```

## Security

Storing secrets this way is better than storing them in your `.bashrc` or in a `.env` auto-loaded by tools like pipenv or direnv because it reduces the numbers of programs with access to your secrets.

It protects against accidental leaks like dumping your environment in a chat system while debugging an issue or a tool writing environment values to a log file.

It *does not* protect against rogue users taking over your machine or rogue programs running on your machine: your secrets are still stored in clear text.

## Installation

- Copy `with-env` to a directory in `$PATH`,
- Create a `~/.config/with-env` directory,
- Create environment files in this directory,
- Optionally, copy completion files from `completions` to the relevant completion place for your shell.

## Usage

<!-- Update with `cog -r README.md` -->
<!-- [[[cog
import subprocess
result = subprocess.run(["with-env", "--help"], capture_output=True, text=True)
cog.outl("```")
cog.out(result.stdout)
cog.outl("```")
]]]-->
```
Usage: with-env [<OPTIONS>] <ENV> <CMD> [ARGS...]
   or: with-env [<OPTIONS>] -l
   or: with-env [<OPTIONS>] -e <ENV>

Load ENV and run CMD with ARGS.

ENV is a file stored in /home/aurelien/.config/with-env.

Options:
  -h, --help          display this usage message and exit.
  -l, --list          list available environments.
  -e, --edit          edit <ENV> file.
  --                  stop parsing options.
```
<!--[[[end]]] -->
